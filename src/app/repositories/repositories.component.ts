import { Component, OnInit } from '@angular/core';
import  {RepositoriesService} from './repositories.service';

@Component({
  selector: 'app-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css']
})



export class RepositoriesComponent implements OnInit {
  
   repositories : any = [];

   repository: Irepository={name:"",description:""};
   newRepository: Irepository={name:"",description:""};

  constructor(private RepoService:RepositoriesService) { }

  ngOnInit() {


 //this.repository={name:"angular 2", description:"descripcion 2"};
 
  // setTimeout(()=>{
  //    	this.repositories=[
  // 	{name:"angular", description:"descripcion 1"},
  // 	{name:"angular 2", description:"descripcion 2"}
  // 	];
  	
  // },3000);

    this.RepoService.getRepos().subscribe((data)=>
    {
    	this.repositories=data.json();

    	this.repository=this.repositories[0];
    	console.log(data.json());
    })
  }

  setMainRepository(repository)
  {
  this.repository=repository;
  }

   addNewRepo(repository)
  {
  this.repositories.unshift(this.newRepository);
  this.newRepository={name:"",description:""};
  }
   
   

}

interface Irepository
{
	name:string,
	description:string
}
